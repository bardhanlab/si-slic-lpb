% from running calculatephe.m
E_asym_opt_params = selfEnergy';

% phe charges from CHARMM force field
q = [     0.5100
   -0.5100
   -0.4700
    0.3100
   -0.1100
    0.0900
    0.0900
    0.0900
   -0.2700
    0.0900
    0.0900
    0.0900
    0.5100
   -0.5100
   -0.4700
    0.3100
    0.0700
    0.0900
   -0.1800
    0.0900
    0.0900
         0
   -0.1150
    0.1150
   -0.1150
    0.1150
   -0.1150
    0.1150
   -0.1150
    0.1150
   -0.1150
    0.1150
  ];
qzero = 0 * q;

% phe single-atom charging free energies
E=[  -6.67354
 -25.7141
 -18.2164
 -5.02364
 -1.67479
 0.268575
 0.231123
 0.240413
 -6.88561
 0.236076
 0.239537
 0.234325
 -7.03194
 -25.4544
 -18.0491
 -3.24099
 0.389057
 0.272644
 -3.13542
 0.238167
 0.25344
 -8.3766e-13
 -1.67777
 0.105415
 -1.74802
 0.0776271
 -1.71333
 0.0867925
 -1.71373
 0.101804
 -1.76353
 0.101094];

% physical constants and MD simulation parameters
phi_static = 10.7;
ksi =2.837;
conv_factor = 332.112;
box_length = 40.0;
correction = 0.5 * ksi * conv_factor / box_length;

% correct out the MD results for changes in MD system net charge
% see the NAMD FEP tutorial for more information.
E_pbc_removed = E - correction * (q.^2 - qzero.^2 );

% alpha = 0.320464
% beta = -46.951476
% gamma = -1.182070
% mu fixed as described in Bardhan12
% phi_static = 10.7

% in case you change the parameters in calculatephe, here are the
% results from using the above parameters
E_asym_opt_params_ref = [ -16.6105
  -23.5958
  -16.9536
  -10.2827
   -0.9892
   -0.9351
   -0.9862
   -0.9267
   -5.8428
   -0.8426
   -0.8921
   -0.8940
  -17.2364
  -23.2698
  -15.4753
   -8.3065
   -0.2955
   -0.7787
   -2.1032
   -0.7945
   -0.7647
         0
   -0.9647
   -1.3657
   -1.0264
   -1.4826
   -1.0276
   -1.4848
   -0.9830
   -1.4052
   -1.0258
   -1.4820];

E_sym_roux = [  -15.1082
  -22.9413
  -13.7235
   -8.1783
   -0.8898
   -0.8481
   -0.8761
   -0.8538
   -5.4598
   -0.7944
   -0.9176
   -0.9124
  -16.5072
  -22.5158
  -12.8253
   -7.2376
   -0.2462
   -0.5258
   -1.7009
   -0.5756
   -0.5631
         0
   -0.8554
   -1.4276
   -0.9492
   -1.6622
   -0.9640
   -1.6748
   -0.8700
   -1.5091
   -0.9551
   -1.6660
];


figure
plot(E_pbc_removed,E_sym_roux,'rx','linewidth',2,'markersize',10)
hold on
plot(E_pbc_removed,E_asym_opt_params+phi_static*q,'bs','linewidth',2,'markersize',10)
plot([2 -35],[2 -35],'k','linewidth',2)
set(gca,'fontsize',16);
xlabel('MD results')
ylabel('Continuum results')
legend('Symmetric model (Roux radii)','SLIC')
legend('Symmetric model (Roux radii)','SLIC','location','southeast')
export_fig phe-atom-by-atom-correlation.pdf -painters -transparent

figure;
plot(1:length(q),E_pbc_removed,'ko','linewidth',2,'markersize',10)
set(gca,'fontsize',16);
hold on
plot(1:length(q),E_sym_roux,'rx','linewidth',2,'markersize',10)
plot(1:length(q),E_asym_opt_params+phi_static*q,'bs','linewidth',2,'markersize',10)
plot(1:length(q),E_pbc_removed,'ko','linewidth',2,'markersize',10)
xlabel('Atom Index');
ylabel('Electrostatic Solvation Free Energy (kcal/mol)')
legend('MD','Symmetric model (Roux radii)','SLIC','location','southeast')
export_fig phe-atom-by-atom.pdf -painters -transparent

figure;
plot(1:length(q),abs(E_sym_roux-E_pbc_removed),'rx','linewidth',2,'markersize',10)
hold on
plot(1:length(q),abs(E_asym_opt_params+phi_static*q-E_pbc_removed),'bs','linewidth',2,'markersize',10)
set(gca,'fontsize',16);
xlabel('Atom Index');
ylabel('Deviation from MD (kcal/mol)')
legend('Symmetric model (Roux radii)','SLIC','location','northeast')
export_fig phe-atom-by-atom-deviation.pdf -painters -transparent
