labels = {'Glu549','Arg539','Lys541','Glu507','Lys546','WT','Arg520','Tyr521',...
	  'Asp550','Arg553','Asp503','Arg517','Lys515','Lys526','Tyr535','Arg542',...
	  'Tyr510','Tyr523'};

WT  = 6;
glu = [4 1];
arg = [12 7 2 16 10]; %12(517) 7(520) 2(539) 16(542) 10(553)
lys = [13 14 3 5]; 
tyr = [17 8 18 15];
asp = [11 9];

newparams=[-295.779774
	-245.479309
	-251.072432
	-297.057657
	-243.714058
	-282.565011
	-246.071564
	-280.119863
	-305.898853
	-275.106876
	-289.900594
	-252.219436
	-247.182965
	-256.61381 
	-250.562188
	-260.190802
	-256.724841
	-283.331982];

origparams =[
    -291.493159
    -245.30151 
    -251.765945
    -292.203325
    -243.79011 
    -280.91718 
    -246.6449  
    -282.42893 
    -301.426842
    -276.277787
    -285.21376 
    -252.053916
    -247.095672
    -256.576358
    -251.978685
    -260.27578 
    -258.449444
    -285.046429];
sym = [
	-326.821433
	-272.482819
	-277.39376 
	-328.05035 
	-270.525231
	-312.297563
	-272.531662
	-307.515477
	-337.055931
	-301.928608
	-321.278469
	-279.004084
	-274.145737
	-283.490314
	-278.477736
	-286.520935
	-284.746385
	-311.148404];


figure;
plot(sym([glu asp tyr arg lys])-sym(WT),'bs','markersize',10,'linewidth',2);
set(gca,'fontsize',16);
xlabel('Index');
ylabel(' \Delta \Delta G^{solv} (kcal/mol)');
hold on
plot(origparams([glu asp tyr arg lys])-origparams(WT),'rx','markersize',10,'linewidth',2);
plot(newparams([glu asp tyr arg lys])-newparams(WT),'ko','markersize',10,'linewidth',2);
legend('Standard PB (SLIC radii)','SLIC (original parameters)',...
       'SLIC (new parameters)','location','southeast');
export_fig bpti-delta-delta-G-solv.pdf  -painters -transparent
return;
% original figure; uses raw order of results in output
figure;
plot(sym([1:5 7:end])-sym(6),'bs','markersize',10,'linewidth',2);
set(gca,'fontsize',16);
xlabel('Index');
ylabel(' \Delta \Delta G (kcal/mol)');
hold on
plot(origparams([1:5 7:end])-origparams(6),'rx','markersize',10,'linewidth',2);
plot(newparams([1:5 7:end])-newparams(6),'ko','markersize',10,'linewidth',2);
legend('Standard PB (SLIC radii)','SLIC (original parameters)',...
       'SLIC (new parameters)','location','southeast');


